package ru.tkbdemo;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.cxf.binding.soap.SoapHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tkbdemo.objects.types.v1.CreateUserRequest;
import ru.tkbdemo.objects.types.v1.CreateUserResponse;
import ru.tkbdemo.service.UserService;

import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;

@Component
public class Endpoint implements Processor {

    @Autowired
    private UserService userService;

    @Override
    public void process(Exchange exchange) throws Exception {
        CreateUserRequest request = exchange.getIn().getBody(CreateUserRequest.class);
        CreateUserResponse response = userService.saveUser(request);
        exchange.getOut().setBody(response);
    }




}
