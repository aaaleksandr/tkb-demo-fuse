package ru.tkbdemo.service;

import ru.tkbdemo.objects.types.v1.CreateUserRequest;
import ru.tkbdemo.objects.types.v1.CreateUserResponse;

public interface UserService {
    CreateUserResponse saveUser(CreateUserRequest request);
}
