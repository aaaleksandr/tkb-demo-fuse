package ru.tkbdemo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tkbdemo.objects.types.v1.CreateUserRequest;
import ru.tkbdemo.objects.types.v1.CreateUserResponse;
import ru.tkbdemo.repository.UserRepository;
import ru.tkbdemo.service.UserService;

import java.math.BigInteger;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public CreateUserResponse saveUser(CreateUserRequest request){
        Long id = userRepository.saveUser(request);
        CreateUserResponse response = new CreateUserResponse();
        response.setId(BigInteger.valueOf(id));
        return response;
    }

}
