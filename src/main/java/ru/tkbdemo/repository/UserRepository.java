package ru.tkbdemo.repository;

import ru.tkbdemo.objects.types.v1.CreateUserRequest;

public interface UserRepository {
    Long saveUser(CreateUserRequest request);
}
