package ru.tkbdemo.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.tkbdemo.objects.types.v1.CreateUserRequest;
import ru.tkbdemo.repository.UserRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private DataSource dataSource;

    @Autowired
    public UserRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Long saveUser(CreateUserRequest request){
        final String SQL = "insert into users(first_name, last_name, login) values (?, ?, ?)";
        Map<String, Object> params = new HashMap<>();

        Long id = null;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, request.getFirstName());
            preparedStatement.setString(2, request.getLastName());
            preparedStatement.setString(3, request.getLogin());

            int executeUpdate = preparedStatement.executeUpdate();

            if (executeUpdate > 0){
                try(ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                    if (resultSet.next()){
                        id = resultSet.getLong(1);
                    }
                } catch (SQLException e){
                    e.printStackTrace();
                }
            }

        } catch (SQLException e){
            e.printStackTrace();
        }
        return id;
    }

}
