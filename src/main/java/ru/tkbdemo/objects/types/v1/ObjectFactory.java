
package ru.tkbdemo.objects.types.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tkbdemo.objects.types.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateUserResponse_QNAME = new QName("http://tkbdemo.ru/objects/types/v1", "createUserResponse");
    private final static QName _CreateUserFault_QNAME = new QName("http://tkbdemo.ru/objects/types/v1", "createUserFault");
    private final static QName _CreateUserRequest_QNAME = new QName("http://tkbdemo.ru/objects/types/v1", "createUserRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tkbdemo.objects.types.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link CreateUserFault }
     * 
     */
    public CreateUserFault createCreateUserFault() {
        return new CreateUserFault();
    }

    /**
     * Create an instance of {@link CreateUserRequest }
     * 
     */
    public CreateUserRequest createCreateUserRequest() {
        return new CreateUserRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tkbdemo.ru/objects/types/v1", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tkbdemo.ru/objects/types/v1", name = "createUserFault")
    public JAXBElement<CreateUserFault> createCreateUserFault(CreateUserFault value) {
        return new JAXBElement<CreateUserFault>(_CreateUserFault_QNAME, CreateUserFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tkbdemo.ru/objects/types/v1", name = "createUserRequest")
    public JAXBElement<CreateUserRequest> createCreateUserRequest(CreateUserRequest value) {
        return new JAXBElement<CreateUserRequest>(_CreateUserRequest_QNAME, CreateUserRequest.class, null, value);
    }

}
